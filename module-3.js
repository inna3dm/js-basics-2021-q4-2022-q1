// Array to List
function arrayToList(array) {
    let prevList = null;
    for (let i = array.length - 1; i >= 0; i--) {
        prevList = {
            value: array[i],
            rest: prevList,
        };
    }
    return prevList;
}

function listToArray(list) {
    let array = [];
    while (list !== null) {
        array.push(list.value);
        list = list.rest;
    }
    return array;
}

// Keys and values to list
function getKeyValuePairs(obj) {
    return Object.entries(obj);
}

// Invert keys and values
function invertKeyValue(obj) {
    return Object.fromEntries(Object.entries(obj).map(a => a.reverse()));
}

// Get all methods from an object
function getAllMethods(obj) {
    let props =  Object.getOwnPropertyNames(obj);
    return props.filter(el => typeof obj[el] === 'function');
}

// Clock
function Clock() {}

Clock.prototype.run = function() {
    this.intervalId = setInterval(function() {
        console.log(new Date().toLocaleTimeString('uk-UA'));
    }, 1000);
}

Clock.prototype.stop = function() {
    clearInterval(this.intervalId);
}

// Groups
class Group {
    items = [];
    add(item) {
        if (!this.has(item)) {
            this.items.push(item);
        }
    }
    delete(item) {
        if (this.has(item)) {
            this.items = this.items.filter(i => i !== item);
        }
    }
    has(item) {
        return this.items.indexOf(item) !== -1;
    }
    static from(array) {
        let group = new Group();
        array.forEach(i => group.add(i));
        return group;
    }
}



