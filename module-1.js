function changeCase(str) {
    return str.split("").map(c => c === c.toUpperCase() ? c.toLowerCase() : c.toUpperCase()).join("");
}

function filterNonUnique(values) {
    return values.filter((el, index, arr) => arr.indexOf(el) === arr.lastIndexOf(el));
}

function alphabetSort(str) {
    return str.split("").sort().join("");
}

function getSecondMinimum(arr) {
    return [...new Set(arr)].sort((a, b) => a - b)[1];
}

function doubleEveryEven(arr) {
    return arr.map(el => el % 2 === 0 ? el * 2 : el);
}

function getArrayElementsPairs(arr1, arr2) {
    return arr1.map(el => arr2.map(e => [el, e])).flat();
}

function deepEqual (obj1, obj2) {
    if (obj1 === obj2) {
        return true;
    }

    if ((typeof obj1 === "object" && obj1 !== null) && (typeof obj2 === "object" && obj2 !== null)) {
        if (Object.keys(obj1).length !== Object.keys(obj2).length) {
            return false;
        }

        for (let prop in obj1) {
            if (obj2.hasOwnProperty(prop)) {
                if (!deepEqual(obj1[prop], obj2[prop]))
                    return false;
            }
        }
        return true;
    }
    return false;
}

function formatDate(value) {
    let date = Array.isArray(value) ? new Date(...value) : new Date(value);
    return date.toLocaleDateString('uk-UA', {day: "2-digit", month: "2-digit", year: "2-digit"});
}
