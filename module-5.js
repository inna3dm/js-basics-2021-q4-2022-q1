// 1. Delay
const delay = timeout => new Promise((resolve) =>
    setTimeout(resolve, timeout));

// 2. Array of promises in series (without async/await)
const runPromisesInSeries = promiseCalls =>
    promiseCalls.reduce((p, next) => p.then(next), Promise.resolve());

// 3. Building Promise.all
function Promise_all(promises) {
    if (!promises.length) {
        return Promise.resolve([]);
    }
    const resolved = new Array(promises.length);
    const results = new Array(promises.length);
    return new Promise((resolve, reject) => {
        for (let i = 0; i < promises.length; i++) {
            const promise = promises[i];
            promise.then(result => {
                results[i] = result;
                resolved[i] = true;
                let allResolved = true;
                for (let resolvedItem of resolved) {
                    if (!resolvedItem) {
                        allResolved = false;
                        break;
                    }
                }
                if (allResolved) {
                    resolve(results);
                }
            }).catch(error => {
                reject(error);
            });
        }
    });
}

// 4. Fibonacci (generator function)
function *fibonacci(n, cur = 0, next = 1) {
    if (n === 0) {
        return cur;
    }
    yield cur;
    yield *fibonacci(n-1, next, cur + next);
}

// 5. Generator helper
function helper(generator) {
    const iterator = generator();
    function iterate(iteration) {
        if (iteration.done) {
            return iteration.value;
        }
        const promise = iteration.value;
        return promise.then(x => iterate(iterator.next(x))).catch(error => iterator.throw(error));
    }
    return iterate(iterator.next());
}
