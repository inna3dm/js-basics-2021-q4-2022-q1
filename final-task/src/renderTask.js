import {getTime} from "./getTime";
import {moveTaskToList} from "./moveTaskToList";
import {updateListOnPage} from "./sortList";

export function renderLists(listName) {
    Object.keys(listName).forEach(key => updateListOnPage(listName[key], key));
}

export function renderTask(listNode, taskValue, creationDate, {completedDate, insertAtTop} = {}) {
    let creationTime = getTime(creationDate);
    let newTask = document.createElement('li');
    newTask.classList.add('card');
    newTask.id = creationDate;
    let checkbox = document.createElement('input');
    checkbox.setAttribute('type', 'checkbox');
    if (listNode.id === 'completedTasks-list') {
        checkbox.checked = true;
    }
    setCheckboxListener(checkbox);

    let text = document.createElement('div');
    text.classList.add('card-text');
    text.textContent = taskValue;

    let timeContainer = document.createElement('div');
    timeContainer.classList.add('time');
    let timeCreated = document.createElement('span');
    timeCreated.classList.add('time-created');
    timeCreated.textContent = creationTime;
    timeContainer.append(timeCreated);

    if (completedDate) {
        let timeCompleted = document.createElement('span');
        timeCompleted.classList.add('time-completed');
        timeCompleted.textContent = getTime(completedDate);
        timeContainer.append(timeCompleted);
    }

    let deleteBtn = document.createElement('button');
    deleteBtn.classList.add('delete-btn');
    newTask.append(checkbox, text, timeContainer, deleteBtn);
    insertAtTop ? listNode.prepend(newTask) : listNode.append(newTask);
}

export function setCheckboxListener(checkbox) {
    checkbox.addEventListener('change', function(e) {
        moveTaskToList(e.target);
    })
}