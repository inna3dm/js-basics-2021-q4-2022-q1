import {getListsFromStorage, setListsToStorage} from "./storage";
import {renderTask} from "./renderTask";

export function addNewTask(input, listEl) {
    let text = input.value;
    let creationDate = +new Date();
    let todoLists = getListsFromStorage();

    todoLists.openTasks.push({id: creationDate, text: text, creationDate: creationDate, status: 'open'});
    setListsToStorage(todoLists);

    renderTask(listEl, text, creationDate, {insertAtTop: true});
    input.value = '';
}