import {getListsFromStorage, setListsToStorage} from "./storage";
import {removeValueFromArray} from "./removeElement";
import {renderTask} from "./renderTask";
import {openList, completedList} from "./constants";

export function moveTaskToList(checkbox) {
    let task = checkbox.closest('li');
    let todoLists = getListsFromStorage(+task.id);

    if (checkbox.checked) {
        completeTask(task, todoLists);
    } else {
        reopenTask(task, todoLists);
    }
    setListsToStorage(todoLists);
}

function completeTask(task, todoLists) {
    let taskId = +task.id;
    let completedDate = +new Date();

    let {text, creationDate} = todoLists.openTasks.find(el => el.id === taskId);
    task.remove();
    removeValueFromArray(todoLists.openTasks, taskId);
    todoLists.completedTasks.push({id: taskId, text, creationDate, completedDate, status: 'completed', });
    renderTask(completedList, text, creationDate, {completedDate, insertAtTop: true});
}

function reopenTask(task, todoLists) {
    let taskId = +task.id;
    let {text, creationDate} = todoLists.completedTasks.find(el => el.id === taskId);
    task.remove();
    removeValueFromArray(todoLists.completedTasks, taskId);
    todoLists.openTasks.push({id: taskId, text, creationDate, status: 'open'});
    renderTask(openList, text, creationDate, {insertAtTop: true});
}