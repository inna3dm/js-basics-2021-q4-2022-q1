import {getListsFromStorage, setListsToStorage} from "./storage";

export function editTask(el) {
    let task = el.closest('li');
    let taskId = +task.id;
    let listId = el.closest('ul').id;
    let textEl = task.querySelector('.card-text');
    let prevValue = textEl.textContent;

    if (!textEl.getElementsByTagName('input').length) {
        let input = document.createElement('input');
        input.type = 'text';
        input.value = prevValue;
        textEl.textContent = '';
        textEl.append(input);
        input.focus();

        input.addEventListener('keyup', function(e) {
            if (e.key === 'Enter') {
                let newValue = input.value;
                textEl.textContent = newValue;
                input.remove();
                let todoLists = getListsFromStorage();
                let list = todoLists[listId.replace('-list', '')];
                list.find(task => task.id === taskId).text = newValue;
                setListsToStorage(todoLists);
            }
            if (e.key === 'Escape') {
                textEl.textContent = prevValue;
                input.remove();
            }
        });
    }
}