export const main = document.getElementById('todo-list-main');
export const newTaskInput = document.getElementById('newTask');
export const addTaskBtn = document.getElementById('add-btn');
export const openList = document.getElementById('openTasks-list');
export const completedList = document.getElementById('completedTasks-list');
export const openListSelect = document.getElementById('openTasks-order-select');
export const completedListSelect = document.getElementById('completedTasks-order-select');
