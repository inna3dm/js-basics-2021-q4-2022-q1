import {getListsFromStorage, setListsToStorage} from "./storage";

export function removeEl(el) {
    let taskId = +el.closest('li').id;
    let listKey = el.closest('ul').id.replace('-list', '');
    let todoLists = getListsFromStorage();
    removeValueFromArray(todoLists[listKey], taskId);
    setListsToStorage(todoLists);
    el.closest('li').remove();
}

export function removeValueFromArray(arr, taskId) {
    let index = arr.indexOf(arr.find(item => item.id === taskId));
    if (index !== -1) {
        arr.splice(index, 1);
    }
    return arr;
}