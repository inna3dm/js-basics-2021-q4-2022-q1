import {main} from "./constants";

export function searchInLists(query) {
    let cardTextNodes = main.querySelectorAll('.card-text');

    if (query) {
        let regExp = new RegExp(query, 'gi');
        cardTextNodes.forEach(textEl =>
        {
            textEl.textContent.match(regExp) ?
                textEl.closest('li').classList.remove('hidden') :
                textEl.closest('li').classList.add('hidden')
        });
    } else {
        cardTextNodes.forEach(textEl => textEl.closest('li').classList.remove('hidden'));
    }
}