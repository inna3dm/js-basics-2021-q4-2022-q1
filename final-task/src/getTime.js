export function getTime(dateMs) {
    return new Date(dateMs).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
}