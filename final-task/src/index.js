import './styles/main.css'
import {getListsFromStorage, setListsToStorage} from "./storage";
import {addNewTask} from "./addNewTask";
import {editTask} from "./editTask";
import {removeEl} from "./removeElement";
import {searchInLists} from "./search";
import {sortList} from "./sortList";
import {clearList} from "./clearList";
import {renderLists} from "./renderTask";
import {
    main, newTaskInput, addTaskBtn, openList, openListSelect, completedListSelect
} from "./constants";

let todoLists;
if (localStorage.hasOwnProperty('todoLists')) {
    todoLists = getListsFromStorage();
    renderLists(todoLists);
} else {
    todoLists = {
        openTasks: [],
        completedTasks: [],
    }
    setListsToStorage(todoLists)
}

setAddListeners();
setEditListener();
setDeleteListeners();
setSearchInputListener();
setSelectSortOptionListener();

function setAddListeners() {
    addTaskBtn.addEventListener('click', function() {
        addNewTask(newTaskInput, openList)
    })
    newTaskInput.addEventListener('keyup', function(e) {
        if (e.key === 'Enter') {
            addNewTask(newTaskInput, openList);
        }
    })
}

function setSelectSortOptionListener() {
    openListSelect.addEventListener('change', function() {
        sortList('openTasks', openListSelect.value);
    });
    completedListSelect.addEventListener('change', function() {
        sortList('completedTasks', completedListSelect.value);
    });
}

function setEditListener() {
    main.addEventListener('dblclick', function(e) {
        editTask(e.target);
    })
}

function setSearchInputListener() {
    document.getElementById('search-field').addEventListener('keyup', function(e) {
        searchInLists(e.target.value);
    })
}

function setDeleteListeners() {
    main.addEventListener('click', function(e) {
        if (e.target.classList.contains('delete-btn')) {
            removeEl(e.target);
        }

        if (e.target.classList.contains('clear-list-btn')) {
            clearList(e.target);
        }
    })
}
