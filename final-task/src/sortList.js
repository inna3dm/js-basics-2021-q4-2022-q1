import {getListsFromStorage, setListsToStorage} from "./storage";
import {clearListOnPage} from "./clearList";
import {renderTask} from "./renderTask";

export function sortList(listName, sortingOption) {
    let todoLists = getListsFromStorage();
    if (!todoLists[listName].length) {
        return;
    }

    let [field, direction] = sortingOption.split('-');
    let dirSign = direction === 'asc' ? 1 : -1;
    if (field === 'text') {
        todoLists[listName].sort((a, b) => dirSign * a[field].localeCompare(b[field]));
    } else if (field === 'creationDate' || field === 'completedDate') {
        todoLists[listName].sort((a, b) => dirSign * (a[field] - b[field]));
    }
    setListsToStorage(todoLists);
    updateListOnPage(todoLists[listName], listName);
}

export function updateListOnPage(list, listName) {
    let listNode = document.getElementById(`${listName}-list`);
    clearListOnPage(listNode);
    list.forEach(task => {
        let {text, creationDate, completedDate} = task;
        renderTask(listNode, text, creationDate, {completedDate})
    });
}