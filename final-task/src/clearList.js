import {getListsFromStorage, setListsToStorage} from "./storage";

export function clearList(btnEl) {
    let listNode = btnEl.closest('.container').querySelector('ul');
    clearListOnPage(listNode);
    let todoLists = getListsFromStorage();
    let listKey = listNode.id.replace('-list', '');
    todoLists[listKey] = [];
    setListsToStorage(todoLists);
}

export function clearListOnPage(listNode) {
    listNode.innerHTML = '';
}