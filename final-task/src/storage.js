export function setListsToStorage(lists) {
    localStorage.setItem('todoLists', JSON.stringify(lists));
}

export function getListsFromStorage() {
    return JSON.parse(localStorage.getItem('todoLists'));
}