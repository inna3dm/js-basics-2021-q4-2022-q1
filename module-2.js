function mergeWords(str) {
    return function(str1 = undefined) {
        if (!str1) {
            return str;
        }
        str += ` ${str1}`;
        return mergeWords(str);
    }
}

function checkUsersValid(validUsers) {
    return function(users) {
        return users.every(user => validUsers.some(validUser => validUser.id === user.id));
    }
}

function countWords(inputWords) {
    return inputWords.reduce(function (acc, curr) {
        acc[curr] ? ++acc[curr] : acc[curr] = 1;
        return acc;
    }, {});
}

function isPalindrome(str) {
    return str === str.split("").reverse().join("") ? "The entry is a palindrome" : "Entry is not a palindrome";
}

function factorial(num) {
    if (num === 0) {
        return 1;
    }
    return num * factorial(num - 1);
}

function amountToCoins(amount, coinsArray) {
    const value = coinsArray.find(coin => amount >= coin);
    return value ? [value, ...amountToCoins(amount - value, coinsArray)] : [];
}

function repeat(fn, num) {
   if (num > 0) {
       fn();
       repeat(fn, num - 1);
   }
}

function reduce(arr, fn, initialValue) {
    if (arr.length > 0) {
        const result = fn(initialValue, arr.shift(), 0, arr);
        return reduce(arr, fn, result);
    }
    return initialValue;
}
