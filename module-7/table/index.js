function setDelListener() {
    document.getElementById('tbody').addEventListener('click', function(e) {
        if (e.target.classList.contains('remove_cell')) {
            e.target.closest('tr').remove();
        }
    })
}

function addRow() {
    let tBody = document.getElementById('tbody');
    let curRow = document.createElement('tr');
    let totalRowsCount = tBody.rows.length;

    for (let i = 0; i < 3; i++) {
        let curCell = document.createElement('td');
        if (i === 2) {
            curCell.className = 'remove_cell';
        }
        let curText = document.createTextNode(i === 2 ? '' : 'Cell' + `${totalRowsCount * 3 + i - 2}`);
        curCell.appendChild(curText);
        curRow.appendChild(curCell);
    }
    tBody.appendChild(curRow);
}

document.getElementById('tbody').addEventListener('dblclick', function(e) {
    let td = e.target.closest('td');
    if (td && td.childNodes[0].tagName !== 'INPUT' && e.target.childNodes.length) {
        let textNode = e.target.childNodes[0];
        let defaultValue = textNode.nodeValue;

        let input = document.createElement('input');
        input.type = 'text';
        input.value = defaultValue;
        input.placeholder = 'Enter text..'
        textNode.replaceWith(input);
        input.focus();
        input.onkeydown = function(e) {
            if (e.key === 'Enter') {
                let savedText = document.createTextNode(input.value);
                input.replaceWith(savedText);
            }
        }
    }
})