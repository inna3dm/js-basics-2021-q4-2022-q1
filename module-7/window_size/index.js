let time = Date.now();
const wait = 1000;
let height, width;
let timeoutHandle;

function getWindowSize() {
    if (time + wait - Date.now() < 0) {
        updateSizeIndicators();
        time = Date.now();
    }
    if (timeoutHandle) {
        clearTimeout(timeoutHandle);
    }
    timeoutHandle = setTimeout(updateSizeIndicators, wait);
}

function updateSizeIndicators() {
    height = window.innerHeight;
    let heightContainer = document.getElementById('height');
    heightContainer.textContent = `Hello, height ${height}`;
    width = window.innerWidth;
    let widthContainer = document.getElementById('width');
    widthContainer.textContent = `Hello, width ${width}`;
}