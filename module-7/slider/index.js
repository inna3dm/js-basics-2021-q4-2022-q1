function addImage() {
  let img = document.createElement('img');
  img.src = document.getElementById('add_url').value;
  img.className = 'slide';
  document.getElementById('slideshow_container').appendChild(img);
  let currentSlide = document.querySelector('#slideshow_container .current-slide');
  if (currentSlide) {
    currentSlide.classList.remove('current-slide');
  }
  img.classList.add('current-slide');
  img.addEventListener('dblclick', function () {
    removeImage(img);
  });
}

function removeImage(img) {
  if (confirm('Really remove?')) {
    plusSlides(1);
    img.remove();
  }
}

let timerHandle;

function resetSliderTimeout() {
  if (timerHandle) {
    clearInterval(timerHandle);
  }

  let interval = parseInt(document.getElementById('add_timer').value);
  if (!isNaN(interval) && interval > 0) {
    timerHandle = setInterval(() => plusSlides(1, true), interval * 1000);
  }
}

function plusSlides(sign, automatic) {
  let slides = [...document.querySelectorAll('#slideshow_container .slide')];
  let currentSlideI = slides.findIndex(slide => slide.classList.contains('current-slide'));
  if (currentSlideI !== -1) {
    let nextSlideI = (slides.length + currentSlideI + sign) % slides.length;
    slides[currentSlideI].classList.remove('current-slide');
    slides[nextSlideI].classList.add('current-slide');
    if (!automatic) {
      resetSliderTimeout();
    }
  }
}

resetSliderTimeout();