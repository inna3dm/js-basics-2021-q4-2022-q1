function bold () {
  let query = document.getElementById('user_input').value;
  let text = document.getElementById('user_text').value;
  let regExp = new RegExp(escapeStr(query), 'g');

  let viewEl = document.getElementById('copied_text');
  viewEl.innerHTML = '';
  let curI = 0;
  for (let match of text.matchAll(regExp)) {
    let prevText = text.substring(curI, match.index);
    viewEl.appendChild(document.createTextNode(prevText));
    let boldEl = document.createElement('b');
    boldEl.textContent = match[0];
    viewEl.appendChild(boldEl);
    curI = match.index + match[0].length;
  }
  viewEl.appendChild(document.createTextNode(text.substring(curI)));
}

function copyText () {
  let textValue = document.getElementById('user_text').value;
  let copiedText = document.getElementById('copied_text');
  copiedText.textContent = textValue;
}

function escapeStr(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}
