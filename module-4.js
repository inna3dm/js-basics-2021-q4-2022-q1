// 1. Point
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    plus(point) {
        return new Point(this.x + point.x, this.y + point.y);
    }
}

// 2. Speaker and Screamer
// ES6
class Speaker {
    constructor(name) {
        this.name = name;
    }
    speak(text) {
        console.log(`${this.name} says ${text}`);
    }
}

class Screamer extends Speaker {
    speak(text) {
        let textCapitalized = text.split('').map(l => l.toUpperCase()).join('');
        console.log(`${this.name} shouts ${textCapitalized}`);
    }
}

// ES5
function Speaker(name) {
    this.name = name;
}

Speaker.prototype.speak = function (text) {
    console.log(`${Speaker.name} says ${text}`);
}

function Screamer(name) {
    Speaker.call(this, name);
}

Screamer.prototype = Object.create(Speaker.prototype);
Screamer.prototype.constructor = Speaker;

Screamer.prototype.speak = function (text) {
    let textCapitalized = text.split('').map(l => l.toUpperCase()).join('');
    console.log(`${this.name} shouts ${textCapitalized}`);
}

// 3. The reading list
class BookList {
    constructor(nextBook, currentBook, lastBook, allBooks) {
        this.nextBook = nextBook || null;
        this.currentBook = currentBook || null;
        this.lastBook = lastBook || null;
        this.allBooks = allBooks || [];
    }
    get readBooksCount() {
        return this.allBooks.filter(b => b.read).length;
    }
    get notReadBooksCount() {
        return this.allBooks.filter(b => !b.read).length;
    }
    add(book) {
        this.allBooks.push(book);
        if (!this.currentBook && !book.read) {
            this.currentBook = book;
        }
        if (!this.nextBook) {
            this.nextBook = this.allBooks.find(b => b !== this.currentBook && !b.read) || null;
        }
    }
    finishCurrentBook() {
        this.currentBook.markAsRead();
        this.lastBook = this.currentBook;
        this.currentBook = this.nextBook;
        this.nextBook = this.allBooks.find(b => b !== this.currentBook && !b.read) || null;
    }
}

class Book {
    constructor ({title, genre, author, read, readDate}) {
        this.title = title;
        this.genre = genre;
        this.author = author;
        this.read = read || false;
        this.readDate = readDate || null;
    }
    markAsRead() {
        this.read = true;
        this.readDate = new Date(Date.now());
    }
}
